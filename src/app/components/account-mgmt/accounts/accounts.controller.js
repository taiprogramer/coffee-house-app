function AccountsController($scope, $element, $attrs, AccountService) {
    var ctrl = this;
    ctrl.accounts = [];

    AccountService.getAccountList().then((v) => {
        ctrl.accounts = v;
    });

    ctrl.deleteAccountById = function (id) {
        AccountService.deleteAccountById(id).then(function (v) {
            return (ctrl.accounts = v);
        });
    };
    ctrl.editAccount = function (id) {
        // get select account by id
        // use scope value, not database value
        var selectedAccount = ctrl.accounts.find(function (account) {
            return account.id === id;
        });
        // call function passed from parent (account-mgmt)
        ctrl.onEdit({ selectedAccount: angular.copy(selectedAccount) });
    };
}

module.exports = AccountsController;
