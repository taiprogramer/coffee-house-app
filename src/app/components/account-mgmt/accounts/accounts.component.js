var accounts = require("./accounts.html").default;

var accountsComponent = {
    template: accounts,
    controller: "AccountsController",
    bindings: {
        onEdit: "&",
    },
};

module.exports = accountsComponent;
