function AccountMgmtController() {
    const ctrl = this;

    ctrl.selectedAccount = {};
    // btn-edit has been clicked via accounts component
    // on-edit binding (&) makes a call to below function
    ctrl.editAccount = function (selectedAccount) {
        ctrl.showEditModal = true;
        ctrl.selectedAccount = selectedAccount;
    };

    ctrl.closeEditModal = function () {
        ctrl.showEditModal = false;
    };
}

module.exports = AccountMgmtController;
