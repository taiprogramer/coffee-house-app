var accountMgmt = require("./account-mgmt.html").default;

var accountMgmtComponent = {
    template: accountMgmt,
    controller: "AccountMgmtController",
};

module.exports = accountMgmtComponent;
