var accountEdit = require("./account-edit.html").default;
var accountEditComponent = {
    template: accountEdit,
    controller: "AccountEditController",
    bindings: {
        onClose: "&",
        account: "=", // selected account for editing
    },
};

module.exports = accountEditComponent;
