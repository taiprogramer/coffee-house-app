function AccountEditController(AccountService) {
    const ctrl = this;
    ctrl.closeEditModal = function () {
        // call parent's function (account-mgmt)
        ctrl.onClose();
    };
    ctrl.confirmUpdate = function () {
        AccountService.updateAccount(ctrl.account).then(
            function (updatedAccount) {
                ctrl.onClose();
            },
            (error) => {}
        );
    };
}

module.exports = AccountEditController;
