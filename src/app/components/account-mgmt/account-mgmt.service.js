function AccountServiceImpl($q) {
    var accounts = [
        {
            id: 1,
            name: "User 1",
            gender: "Male",
            role: "Admin",
            phone: "0123456789",
            email: "user1@us.mail",
        },
        {
            id: 2,
            name: "User 2",
            gender: "Female",
            role: "User",
            phone: "0123456780",
            email: "user2@us.mail",
        },
        {
            id: 3,
            name: "User 3",
            gender: "Female",
            role: "User",
            phone: "0123456781",
            email: "user3@us.mail",
        },
        {
            id: 4,
            name: "User 4",
            gender: "Male",
            role: "User",
            phone: "0123456782",
            email: "user4@us.mail",
        },
        {
            id: 5,
            name: "User 5",
            gender: "Male",
            role: "User",
            phone: "0123456783",
            email: "user5@us.mail",
        },
        {
            id: 6,
            name: "User 6",
            gender: "Female",
            role: "Admin",
            phone: "0123456784",
            email: "user6@us.mail",
        },
    ];
    return {
        getAccountList: function () {
            var deferred = $q.defer();
            setTimeout(function () {
                deferred.resolve(accounts);
            }, 500);
            return deferred.promise;
        },
        deleteAccountById: function (id) {
            var deferred = $q.defer();
            setTimeout(function () {
                var index = accounts.findIndex((account) => account.id == id);
                // remove 1 element from $index position
                for (var i = 0; i < accounts.length; i++) {
                    var account = accounts[i];
                    if (account.id === id) {
                        accounts.splice(index, 1);
                        break;
                    }
                }
                deferred.resolve(accounts);
            }, 500);
            return deferred.promise;
        },
        updateAccount: function (newAccount) {
            var deferred = $q.defer();
            setTimeout(function () {
                for (var i = 0; i < accounts.length; i++) {
                    var account = accounts[i];
                    if (account.id === newAccount.id) {
                        accounts[i] = newAccount;
                        break;
                    }
                }
                deferred.resolve(newAccount);
            }, 500);
            return deferred.promise;
        },
    };
}

module.exports = AccountServiceImpl;
