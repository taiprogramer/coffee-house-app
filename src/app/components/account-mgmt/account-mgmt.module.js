var angular = require("angular");
var accountMgmtComponent = require("./account-mgmt.component");
var AccountMgmtController = require("./account-mgmt.controller");
var AccountsController = require("./accounts/accounts.controller");
var AccountEditController = require("./account-edit/account-edit.controller");
var accountsComponent = require("./accounts/accounts.component");
var AccountServiceImpl = require("./account-mgmt.service");
var accountEditComponent = require("./account-edit/account-edit.component");

angular
    .module("components.account-mgmt", [require("angular-utils-pagination")])
    .controller("AccountMgmtController", AccountMgmtController)
    .controller("AccountsController", AccountsController)
    .controller("AccountEditController", AccountEditController)
    .component("accountMgmt", accountMgmtComponent)
    .component("accounts", accountsComponent)
    .component("accountEdit", accountEditComponent)
    .factory("AccountService", AccountServiceImpl);
