function ProductsController() {
    var ctrl = this;
    ctrl.editProduct = function (product) {
        ctrl.onEdit({ product: product });
    };

    var productForDelete = {};
    ctrl.deleteProduct = function (product) {
        ctrl.confirmDialogShow = true;
        productForDelete = product;
    };

    ctrl.getConfirmText = function () {
        return "Are you sure you want to delete " + productForDelete.name + "?";
    };

    ctrl.deleteSelectedProduct = function () {
        ctrl.onDelete({ product: productForDelete });
        ctrl.hideConfirmDialog();
    };

    ctrl.hideConfirmDialog = function () {
        ctrl.confirmDialogShow = false;
    };
}

module.exports = ProductsController;
