var products = require("./products.html").default;

var productsComponent = {
    template: products,
    controller: "ProductsController",
    bindings: {
        products: "<",
        onEdit: "&",
        onDelete: "&",
    },
};

module.exports = productsComponent;
