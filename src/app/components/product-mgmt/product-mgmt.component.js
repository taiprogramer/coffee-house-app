var productMgmt = require("./product-mgmt.html").default;

var productMgmtComponent = {
    template: productMgmt,
    controller: "ProductMgmtController",
};

module.exports = productMgmtComponent;
