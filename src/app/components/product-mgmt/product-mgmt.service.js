function ProductServiceImpl($q) {
    var products = [
        {
            id: 1,
            name: "Camramel Macchiato",
            imageURL: "./assets/icons/coffee-svgrepo-com.svg",
            category: {
                id: 1,
                name: "Cafe",
            },
            sizes: [
                {
                    id: 2,
                    name: "M",
                    price: 100,
                },
            ],
            description: "Dummy description",
        },
        {
            id: 2,
            name: "Ca phe sua da",
            imageURL: "./assets/icons/coffee-svgrepo-com.svg",
            category: {
                id: 1,
                name: "Cafe",
            },
            sizes: [
                {
                    id: 2,
                    name: "M",
                    price: 100,
                },
                {
                    id: 3,
                    name: "L",
                    price: 150,
                },
            ],
            description: "Dummy description",
        },
        {
            id: 3,
            name: "Ca phe da",
            imageURL: "./assets/icons/coffee-svgrepo-com.svg",
            category: {
                id: 1,
                name: "Cafe",
            },
            sizes: [
                {
                    id: 2,
                    name: "M",
                    price: 75,
                },
                {
                    id: 3,
                    name: "L",
                    price: 100,
                },
            ],
            description: "Dummy description",
        },
    ];

    var categories = [
        { id: 1, name: "Cafe" },
        { id: 2, name: "No cafe" },
    ];

    var sizes = [
        { id: 1, name: "S" },
        { id: 2, name: "M" },
        { id: 3, name: "L" },
    ];
    return {
        getProductList: function () {
            var deferred = $q.defer();
            setTimeout(function () {
                deferred.resolve(products);
            }, 500);
            return deferred.promise;
        },
        getCategories: function () {
            var deferred = $q.defer();
            setTimeout(function () {
                deferred.resolve(categories);
            }, 500);
            return deferred.promise;
        },
        getSizes: function () {
            var deferred = $q.defer();
            setTimeout(function () {
                deferred.resolve(sizes);
            }, 500);
            return deferred.promise;
        },
        saveOrUpdateProduct: function (product) {
            var defered = $q.defer();
            if (product.id === undefined) {
                // new product
                product.id = products.length + 1;
                products.push(product);
                defered.resolve(product);
                return defered.promise;
            }
            // existing product
            for (var i = 0; i < products.length; i++) {
                if (product.id === products[i].id) {
                    products[i] = product;
                }
            }
            defered.resolve(product);
            return defered.promise;
        },
        deleteProductById: function (id) {
            var defered = $q.defer();
            var product = {};
            for (var i = 0; i < products.length; i++) {
                product = products[i];
                if (product.id === id) {
                    defered.resolve(products.splice(i, 1));
                    break;
                }
            }
            return defered.promise;
        },
    };
}

module.exports = ProductServiceImpl;
