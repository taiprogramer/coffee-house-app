function ProductEditModalController(ProductService) {
    var ctrl = this;
    ctrl.confirm = function () {
        // add attribute `name` to category
        ctrl.product.category.name =
            ctrl.categories[ctrl.product.category.id - 1].name;
        // update size `name` based on size_id & `sizes` list
        ctrl.product.sizes = ctrl.product.sizes.map(function (size) {
            size.name = ctrl.sizes[size.id - 1].name;
            return size;
        });

        ctrl.onConfirm({ product: ctrl.product });
    };
    ctrl.cancel = function () {
        ctrl.onClose();
    };
}

module.exports = ProductEditModalController;
