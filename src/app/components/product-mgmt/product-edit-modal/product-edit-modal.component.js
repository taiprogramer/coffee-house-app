var productEditModal = require("./product-edit-modal.html").default;

var productEditModalComponent = {
    template: productEditModal,
    controller: "ProductEditModalController",
    bindings: {
        product: "<",
        onConfirm: "&",
        onClose: "&",
        // optimize performance
        categories: "<", // list of all available categories
        sizes: "<", // list of all available sizes
    },
};

module.exports = productEditModalComponent;
