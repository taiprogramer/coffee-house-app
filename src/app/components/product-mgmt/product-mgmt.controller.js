function ProductMgmtController(ProductService) {
    var ctrl = this;
    ctrl.title = "Product Management";
    ctrl.products = [];
    // hide edit modal by default
    ctrl.showEditModal = false;

    ProductService.getCategories().then(function (v) {
        ctrl.categories = v;
    });
    ProductService.getSizes().then(function (v) {
        ctrl.sizes = v;
    });

    var loadProducts = function () {
        ProductService.getProductList().then(
            function (v) {
                // preprocessing product list
                ctrl.products = v.map(function (product) {
                    // turn sizes array to string
                    // Example
                    // sizes: [{size: "M", price: 100}, {size: "L", price: 150}]
                    // will return: M L
                    product.stringSizes = product.sizes
                        .map(function (s) {
                            return s.name;
                        })
                        .join(" ");
                    return product;
                });
            },
            function (e) {}
        );
    };

    var setSelectedProduct = function (product) {
        if (product === undefined) {
            product = {};
        }

        if (product.sizes === undefined) {
            product.sizes = [{ id: 1, name: "S", price: 0 }];
        }

        // preprocessing for use it in value of HTML Select element (ng-model)
        if (product.category !== undefined) {
            product.category.id = String(product.category.id);
        }

        product.sizes = product.sizes.map(function (size) {
            size.id = String(size.id);
            return size;
        });

        ctrl.product = angular.copy(product);
    };

    /**
     * Update or create new product.
     * Setup correct selected product & show product-edit-modal.
     * Trigger when button "Add" or "Edit" click (on `feature-bar` or
     * `products`).
     * Used by output binding from other component. (&)
     */
    ctrl.updateOrNewProduct = function (product) {
        setSelectedProduct(product);
        ctrl.showEditModal = true;
    };

    /**
     * Close edit modal.
     * Trigger when update successfully or "Cancel" button click
     * (`product-edit-modal:on-close`).
     * Output binding.
     */
    ctrl.closeEditModal = function () {
        ctrl.showEditModal = false;
    };

    // trigger when confirm button on edit modal click
    ctrl.confirmUpdateProduct = function (product) {
        ProductService.saveOrUpdateProduct(product).then(function (_) {
            loadProducts();
            ctrl.closeEditModal();
        });
    };

    ctrl.deleteProduct = function (product) {
        ProductService.deleteProductById(product.id).then(function (_) {
            loadProducts();
        });
    };

    loadProducts();

    // selected product for update or create
    setSelectedProduct({});
}

module.exports = ProductMgmtController;
