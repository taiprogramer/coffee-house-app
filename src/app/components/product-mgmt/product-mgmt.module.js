var angular = require("angular");
var productMgmtComponent = require("./product-mgmt.component");
var productsComponent = require("./products/products.component");
var productEditModalComponent = require("./product-edit-modal/product-edit-modal.component");
var ProductMgmtController = require("./product-mgmt.controller");
var ProductEditModalController = require("./product-edit-modal/product-edit-modal.controller");
var ProductsController = require("./products/products.controller");
var ProductServiceImpl = require("./product-mgmt.service");

angular
    .module("components.product-mgmt", [])
    .controller("ProductMgmtController", ProductMgmtController)
    .controller("ProductEditModalController", ProductEditModalController)
    .controller("ProductsController", ProductsController)
    .component("productMgmt", productMgmtComponent)
    .component("products", productsComponent)
    .component("productEditModal", productEditModalComponent)
    .factory("ProductService", ProductServiceImpl);
