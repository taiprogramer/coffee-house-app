var auth = require("./auth.html").default;

const authComponent = {
    template: auth,
    controller: "AuthController",
};

module.exports = authComponent;
