function AuthServiceImpl($q) {
    var accounts = [
        {
            id: 1,
            username: "user1",
            password: "user1",
            name: "User 1",
            gender: "Male",
            role: "Admin",
            phone: "0123456789",
            email: "user1@us.mail",
        },
        {
            id: 2,
            username: "user2",
            password: "user2",
            name: "User 2",
            gender: "Female",
            role: "User",
            phone: "0123456780",
            email: "user2@us.mail",
        },
        {
            id: 3,
            username: "user3",
            password: "user3",
            name: "User 3",
            gender: "Female",
            role: "User",
            phone: "0123456781",
            email: "user3@us.mail",
        },
        {
            id: 4,
            username: "user4",
            password: "user4",
            name: "User 4",
            gender: "Male",
            role: "User",
            phone: "0123456782",
            email: "user4@us.mail",
        },
        {
            id: 5,
            username: "user5",
            password: "user5",
            name: "User 5",
            gender: "Male",
            role: "User",
            phone: "0123456783",
            email: "user5@us.mail",
        },
        {
            id: 6,
            username: "user6",
            password: "user6",
            name: "User 6",
            gender: "Female",
            role: "Admin",
            phone: "0123456784",
            email: "user6@us.mail",
        },
    ];
    return {
        login: function (username, password) {
            var deferred = $q.defer();
            foundAccounts = accounts.filter(function (account) {
                return (
                    account.username === username &&
                    account.password === password
                );
            });
            if (foundAccounts.length !== 1) {
                deferred.reject("Incorrect username or password.");
                return deferred.promise;
            }
            foundAccounts[0].password = undefined;
            deferred.resolve(foundAccounts[0]);
            return deferred.promise;
        },
    };
}

module.exports = AuthServiceImpl;
