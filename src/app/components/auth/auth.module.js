var angular = require("angular");
var AuthServiceImpl = require("./auth.service");
var AuthController = require("./auth.controller");
const authComponent = require("./auth.component");

angular
    .module("components.auth", [])
    .controller("AuthController", AuthController)
    .component("loginPage", authComponent)
    .factory("AuthService", AuthServiceImpl);
