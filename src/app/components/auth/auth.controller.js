function AuthController($timeout, AuthService) {
    var ctrl = this;
    ctrl.title = "The coffee house";
    ctrl.login = function ($event) {
        // prevent form from reload
        $event.preventDefault();
        AuthService.login(ctrl.username, ctrl.password).then(
            function (account) {
                window.history.replaceState({}, "", "/admin/account-mgmt");
            },
            function (error) {
                // display alert in 2 seconds
                ctrl.loginError = error;
                $timeout(function () {
                    ctrl.loginError = undefined;
                }, 2000);
            }
        );
    };
}

module.exports = AuthController;
