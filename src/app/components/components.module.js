var angular = require("angular");
require("./account-mgmt/account-mgmt.module");
require("./product-mgmt/product-mgmt.module");
require("./auth/auth.module");

angular.module("components", [
    "components.account-mgmt",
    "components.product-mgmt",
    "components.auth",
]);
