var featureBar = require("./feature-bar.html").default;
var featureBarComponent = {
    template: featureBar,
    bindings: {
        title: "<",
        onAdd: "&",
    },
};

module.exports = featureBarComponent;
