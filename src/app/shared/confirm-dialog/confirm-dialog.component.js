var confirmDialog = require("./confirm-dialog.html").default;
var confirmDialogComponent = {
    template: confirmDialog,
    bindings: {
        title: "@",
        bodyText: "<",
        cancelText: "@",
        confirmText: "@",
        onCancel: "&",
        onConfirm: "&",
    },
};

module.exports = confirmDialogComponent;
