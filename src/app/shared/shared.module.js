var angular = require("angular");
var headerBarComponent = require("./header-bar/header-bar.component");
var featureBarComponent = require("./feature-bar/feature-bar.component");
var HeaderBarController = require("./header-bar/header-bar.controller");
var confirmDialogComponent = require("./confirm-dialog/confirm-dialog.component");

angular
    .module("shared", [])
    .controller("HeaderBarController", HeaderBarController)
    .component("headerBar", headerBarComponent)
    .component("confirmDialog", confirmDialogComponent)
    .component("featureBar", featureBarComponent);
