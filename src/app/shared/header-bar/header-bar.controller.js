var userSvg = require("Assets/icons/user-svgrepo-com.svg");

function HeaderBarController() {
    var ctrl = this;
    ctrl.avatar = userSvg;
}

module.exports = HeaderBarController;
