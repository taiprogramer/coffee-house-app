var headerBar = require("./header-bar.html").default;

var headerBarComponent = {
    controller: "HeaderBarController",
    template: headerBar,
};

module.exports = headerBarComponent;
