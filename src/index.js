var angular = require("angular");
var myapp = require("./app/myapp.html").default;

require("./lib/bootstrap.min.css");
require("./app/components/components.module");
require("./app/shared/shared.module");

angular
    .module("myApp", ["components", "shared", require("angular-route")])
    .component("myApp", {
        template: myapp,
    })
    .config([
        "$routeProvider",
        "$locationProvider",
        function ($routeProvider, $locationProvider) {
            $routeProvider
                .when("/admin/product-mgmt", {
                    template: "<product-mgmt></product-mgmt>",
                })
                .when("/admin/account-mgmt", {
                    template: "<account-mgmt></account-mgmt>",
                })
                .when("/admin", {
                    template: "<login-page></login-page>",
                });
            // for clean URLs, required base tag on index.html
            $locationProvider.html5Mode(true);
        },
    ]);
